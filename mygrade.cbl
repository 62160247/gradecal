       IDENTIFICATION DIVISION.
       PROGRAM-ID. MYGRADE.
       AUTHOR. JARUPHONG.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT MY-GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD  MY-GRADE-FILE.
       01 MY-GRADE-DETAIL.
          88 END-OF-MY-GRADE-FILE             VALUE HIGH-VALUE.
       05 SUBJECT-ID               PIC X(6).
          05 SUBJECT-NAME          PIC X(50).
          05 CRD                   PIC 9.
          05 GRADE                 PIC 99.
           
       FD  AVG-GRADE-FILE.
       01 AVG                      PIC 99V999 VALUE ZEROS.
       01 AVG-SCI                  PIC 99V999 VALUE ZEROS.
       01 AVG-CS                   PIC 99V999 VALUE ZEROS.
       

       WORKING-STORAGE SECTION.
       01 GRADE-TO-NUMBER          PIC 9V9.
       01 SUM-CRD                  PIC 999    VALUE ZEROS.
       01 SUM-GRADE-CRD            PIC 999V99 VALUE ZEROS.

       01 SUM-CRD-SCI              PIC 999    VALUE ZEROS.
       01 SUM-GRADE-CRD-SCI        PIC 999V99 VALUE ZEROS.

       01 SUM-CRD-CS               PIC 999    VALUE ZEROS.
       01 SUM-GRADE-CRD-CS         PIC 999V99 VALUE ZEROS.

       01 RESULT.         
          05 AVG-GRADE             PIC 99V999 VALUE ZEROS.
          05 AVG-SCI-GRADE         PIC 99V999 VALUE ZEROS.
          05 AVG-CS-GRADE          PIC 99V999 VALUE ZEROS.
        PROCEDURE DIVISION .
       000-BEGIN.
           OPEN INPUT MY-GRADE-FILE 
           PERFORM UNTIL END-OF-MY-GRADE-FILE
                   READ MY-GRADE-FILE
                   AT END
                      SET END-OF-MY-GRADE-FILE TO TRUE
                   END-READ
                   IF NOT END-OF-MY-GRADE-FILE THEN
                      PERFORM 001-PROCESS
                   END-IF
           END-PERFORM
           COMPUTE AVG-GRADE = SUM-GRADE-CRD / SUM-CRD 
           DISPLAY "AVG-GRADE = " AVG-GRADE
           DISPLAY "SUM-CRD = " SUM-CRD
           COMPUTE AVG-SCI-GRADE = SUM-GRADE-CRD-SCI / SUM-CRD-SCI
           DISPLAY "AVG-SCI-GRADE = " AVG-SCI-GRADE
           COMPUTE AVG-CS-GRADE = SUM-GRADE-CRD-CS / SUM-CRD-CS 
           DISPLAY "AVG-CS-GRADE = " AVG-CS-GRADE    
           CLOSE MY-GRADE-FILE
           PERFORM 002-WRITE THRU 003-EXIT  
           GOBACK
           .
       001-PROCESS.
           EVALUATE TRUE 
           WHEN GRADE = "A"
                MOVE 4 TO GRADE-TO-NUMBER 
           WHEN GRADE = "B+"
                MOVE 3.5 TO GRADE-TO-NUMBER 
           WHEN GRADE = "B"
                MOVE 3 TO GRADE-TO-NUMBER 
           WHEN GRADE = "C+"
                MOVE 2.5 TO GRADE-TO-NUMBER 
           WHEN GRADE = "C"
                MOVE 2 TO GRADE-TO-NUMBER 
           WHEN GRADE = "D+"
                MOVE 1.5 TO GRADE-TO-NUMBER  
           WHEN GRADE = "D"
                MOVE 1 TO GRADE-TO-NUMBER 
           WHEN OTHER
                MOVE 0 TO GRADE-TO-NUMBER 
           END-EVALUATE
           COMPUTE SUM-GRADE-CRD = SUM-GRADE-CRD +(CRD * GRADE-TO-NUMBER
              )
           COMPUTE SUM-CRD = SUM-CRD + CRD
           IF SUBJECT-ID(1:1) = 3 THEN
              COMPUTE SUM-GRADE-CRD-SCI = SUM-GRADE-CRD-SCI
                 +(CRD * GRADE-TO-NUMBER)
              COMPUTE SUM-CRD-SCI = SUM-CRD-SCI + CRD 
           END-IF
           IF SUBJECT-ID(1:2) = 31 THEN
              COMPUTE SUM-GRADE-CRD-CS = SUM-GRADE-CRD-CS
                 +(CRD * GRADE-TO-NUMBER)
              COMPUTE SUM-CRD-CS = SUM-CRD-CS + CRD 
           END-IF 
 
           .
       002-WRITE.
           OPEN OUTPUT AVG-GRADE-FILE
           MOVE AVG-GRADE TO AVG 
           WRITE AVG

           MOVE AVG-SCI-GRADE TO AVG-SCI
           WRITE AVG-SCI
           
           MOVE AVG-CS-GRADE TO AVG-CS
           WRITE AVG-CS

           CLOSE AVG-GRADE-FILE
           .
       003-EXIT.
           EXIT.